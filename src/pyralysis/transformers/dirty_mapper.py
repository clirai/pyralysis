from dataclasses import dataclass, field
from typing import List, Tuple, Union

import astropy.units as un
import dask.array as da
import numpy as np
import xarray as xr
from more_itertools import locate

from ..fft import ifft2
from ..reconstruction import Image
from ..units import array_unit_conversion
from ..utils.padding import remove_padding
from .gridder import Gridder


def weight_bincount(x, weights, minlength: int = None):
    """Helper to perform a bincount ensuring that the index array x and the
    weights array are rechunked with a matching chunk structure. If the weights
    array is multi-dimensional (e.g. shape (n, ncorrs)), it is assumed that the
    first dimension matches the length of x, and the bincount is performed for
    each extra dimension separately.

    Parameters
    ----------
    x : dask.array.Array
        The index array. Expected shape: (n,).
    weights : dask.array.Array
        The weights array. Can be 1D (shape (n,)) or multi-dimensional (e.g. (n, ncorrs)).
    minlength : int, optional
        The minimum length for the output array.

    Returns
    -------
    dask.array.Array
        If weights is 1D, returns a 1D array from da.bincount.
        If weights is multi-dimensional, returns an array where the bincount was computed
        for each extra dimension and the results are stacked on the last axis.
    """
    # If weights is multi-dimensional (more than one axis), process each extra dimension separately.
    if weights.ndim > 1:
        # Assume that the first axis of weights corresponds to x.
        results = []
        # Loop over the second axis (e.g. correlations).
        for i in range(weights.shape[-1]):
            # Recursively call weight_bincount on the 1D slice weights[:, i]
            result_i = weight_bincount(x, weights[:, i], minlength)
            results.append(result_i)
        return da.stack(results, axis=-1)

    # Otherwise, weights is 1D.
    # Flatten both arrays.
    weights_flat = weights.ravel()
    x_flat = x.ravel()

    # Extract the chunk tuple from the flattened weights.
    weights_chunks = weights_flat.chunks[0]

    # Determine target chunks: if the chunks from weights add up to x_flat's length, use them;
    # otherwise, fall back to a single chunk.
    if sum(weights_chunks) == x_flat.shape[0]:
        target_chunks = weights_chunks
    else:
        target_chunks = (x_flat.shape[0], )

    # Rechunk both x_flat and weights_flat using the same target_chunks.
    x_flat = x_flat.rechunk(target_chunks)
    weights_flat = weights_flat.rechunk(target_chunks)

    # Now perform the bincount.
    return da.bincount(x_flat, weights=weights_flat, minlength=minlength)


def complex_bincount(x, weights, minlength: int = None):
    """Compute a complex-valued bincount by separately counting the real and
    imaginary parts.

    This function works when:
      - x is a 1D index array (or has an extra singleton dimension, e.g. (n,1)).
      - weights is a complex dask array whose first dimension matches x.
      If weights has extra dimensions (e.g. (n, ncorr)), the bincount is computed for each channel.

    Parameters
    ----------
    x : dask.array.Array
        The index array.
    weights : dask.array.Array
        The complex weights array.
    minlength : int, optional
        The minimum length for the output array.

    Returns
    -------
    dask.array.Array
        A complex-valued array combining the real and imaginary bincounts; if weights is multi-dimensional,
        the results are stacked along the last axis.
    """
    # If weights is multi-dimensional (e.g. shape (n, ncorr)), process each channel separately.
    if weights.ndim > 1:
        results = []
        # Loop over the last axis (assumed to be the correlation/channel axis)
        for i in range(weights.shape[-1]):
            # Call complex_bincount recursively for each 1D slice weights[:, i]
            result_i = complex_bincount(x, weights[:, i], minlength)
            results.append(result_i)
        return da.stack(results, axis=-1)

    # Now we are in the 1D case.
    # Flatten both arrays.
    weights_flat = weights.ravel()
    x_flat = x.ravel()

    # Extract the chunking tuple from weights_flat.
    weights_chunks = weights_flat.chunks[0]

    # Determine the target chunking.
    # If the sum of the weights chunks equals the length of x_flat, we use them;
    # otherwise, fall back to a single chunk.
    if sum(weights_chunks) != x_flat.shape[0]:
        target_chunks = (x_flat.shape[0], )
    else:
        target_chunks = weights_chunks

    # Rechunk both arrays using the same target_chunks.
    # (Note: pass target_chunks directly, not as (target_chunks,))
    x_flat = x_flat.rechunk(target_chunks)
    weights_flat = weights_flat.rechunk(target_chunks)

    # Separate weights into real and imaginary parts.
    real_w = weights_flat.real
    imag_w = weights_flat.imag

    # Compute the bincounts.
    real_count = da.bincount(x_flat, weights=real_w, minlength=minlength)
    imag_count = da.bincount(x_flat, weights=imag_w, minlength=minlength)

    return real_count + 1j * imag_count


@dataclass(init=False, repr=True)
class DirtyMapper(Gridder):
    """DirtyMapper is responsible for transforming visibility data into dirty
    images.

    It inherits from the Gridder class and supports both a simple gridding approach
    and one that uses a convolutional kernel (ckernel). It maps the gridded visibilities
    and weights into images for each Stokes parameter.

    Attributes
    ----------
    stokes : list of str
        List of Stokes parameters to reconstruct (e.g. ["I", "Q", "U", "V"]).
    data_column : str
        The column name in the input dataset to use (e.g. "CORRECTED_DATA" or "DATA").
    uvgridded_visibilities : dask.array.Array
        The Dask array holding gridded complex visibilities.
    uvgridded_weights : dask.array.Array
        The Dask array holding gridded weights.
    chunks : int or tuple or dict or str
        Chunk sizes for arrays containing gridded data.
    sum_wt : np.ndarray
        Sum of the weights (per Stokes) after gridding.
    max_psf : np.ndarray
        Maximum value of the dirty beam per Stokes.
    """
    stokes: Union[List[str], str] = None
    data_column: str = None
    # Public dask array to store gridded visibilities
    uvgridded_visibilities: da.Array = field(init=False, repr=True, default=None)
    # Public dask array to store gridded weights
    uvgridded_weights: da.Array = field(init=False, repr=True, default=None)
    # Chunks for gridded arrays
    chunks: Union[int, Tuple[int, int, int], str, dict] = field(init=False, repr=True, default=None)

    def __init__(
        self,
        stokes: Union[List[str], str] = None,
        data_column: str = None,
        chunks: Union[int, Tuple[int, int, int], dict] = None,
        **kwargs
    ):
        """Initialize a DirtyMapper instance.

        Parameters
        ----------
        stokes : list or str, optional
            The Stokes parameters to reconstruct. If a string, it will be split on commas.
        data_column : str, optional
            The column name to use from the input dataset. Defaults based on dataset attributes.
        chunks : int, tuple, dict, or str, optional
            Desired chunk sizes for gridded arrays. If None, "auto" is used.
        kwargs : dict
            Additional keyword arguments passed to the parent class (Gridder).
        """

        super().__init__(**kwargs)
        self.stokes = stokes

        if chunks is None:
            self.chunks = "auto"
        else:
            self.chunks = chunks

        # Choose the correct data column.
        if data_column is None:
            if self.input_data.corrected_column_present:
                self.data_column = "CORRECTED_DATA"
            else:
                self.data_column = "DATA"
        else:
            self.data_column = data_column

        if self.stokes is None:
            self.stokes = ["I", "Q", "U", "V"]
        else:
            if isinstance(self.stokes, str):
                self.stokes = self.stokes.split(",")

        # We sort stokes by alphabetical order
        self.stokes.sort()

        if self.imsize is None:
            self.uvgridded_visibilities = None
            self.uvgridded_weights = None
        else:
            self.uvgridded_visibilities = da.zeros(
                (len(self.stokes), self._padded_grid_size[0], self._padded_grid_size[1]),
                dtype=np.complex64,
                chunks=self.chunks
            )
            self.uvgridded_weights = da.zeros(
                (len(self.stokes), self._padded_grid_size[0], self._padded_grid_size[1]),
                dtype=np.float32,
                chunks=self.chunks
            )

        self.sum_wt = np.zeros(len(self.stokes), dtype=np.float32)
        self.max_psf = np.zeros(len(self.stokes), dtype=np.float32)

    def transform(self) -> Tuple[Image, Image]:
        """Perform the full gridding transformation of the input measurement
        sets into dirty images.

        Processing Steps:
          1. Extract uvw, weight, data, and flag from each measurement set.
          2. For non-Hermitian data, duplicate the visibilities (and adjust weights).
          3. Repeat arrays along the channel axis (nchans) and convert uvw into lambda units.
          4. Reshape arrays so that time and channel axes are combined.
          5. Compute pixel coordinates (u_pix, v_pix) using _calculate_uv_pix.
          6. If using a convolutional kernel (ckernel_object not None):
             - Compute convolved index arrays and apply the kernel.
             - Ravel (flatten) the kernel and collapse extra dimensions.
          7. Else (no kernel): Compute a 1D index and mask invalid entries.
          8. Use safe 1D routines (weight_bincount and complex_bincount) to grid visibilities.
          9. Reshape the gridded data into the padded grid.
         10. Combine gridded data into Stokes parameters.
         11. Normalize weights and compute an IFFT to produce dirty images and beams.
         12. Apply any padding removal and gridding correction if needed.
         13. Return Image objects for the dirty maps and beams.

        Returns
        -------
        Tuple[Image, Image]
            The dirty maps and dirty beams as Image objects.
        """
        idx_I = list(locate(self.stokes, lambda x: x == "I"))
        idx_Q = list(locate(self.stokes, lambda x: x == "Q"))
        idx_U = list(locate(self.stokes, lambda x: x == "U"))
        idx_V = list(locate(self.stokes, lambda x: x == "V"))

        padded_grid_size = self._padded_grid_size[0] * self._padded_grid_size[1]
        kernel = None
        if self.ckernel_object is not None:
            kernel = self.ckernel_object.kernel(
                imsize=(self.ckernel_object.size, self.ckernel_object.size), half=True
            ).compute()
            kernel_flat = kernel.ravel()

        for ms in self.input_data.ms_list:
            pol_id = ms.polarization_id
            spw_id = ms.spw_id
            nchans = self.input_data.spws.nchans[spw_id]
            equiv = self.input_data.spws.equivalencies[spw_id]

            uvw = ms.visibilities.uvw.data * un.m
            weight = ms.visibilities.imaging_weight.data
            data = ms.visibilities.dataset[self.data_column].data
            flag = ms.visibilities.flag.data

            # If hermitian_symmetry flag is False, then we duplicate the data since datasets only provide half of it
            if not self.hermitian_symmetry:
                uvw = da.concatenate((uvw, -uvw), axis=0)
                weight = da.concatenate((weight, weight), axis=0)
                data = da.concatenate((data, data.conj()), axis=0)
                flag = da.concatenate((flag, flag), axis=0)

                weight *= 0.5  # Scale weights accordingly

            ncorrs = self.input_data.polarization.ncorrs[pol_id]
            corr_names = self.input_data.polarization.corrs_string[pol_id]

            uvw_broadcast = uvw[:, None, :]

            uvw_broadcast = da.repeat(uvw_broadcast, nchans, axis=1)
            uvw_lambdas = array_unit_conversion(
                array=uvw_broadcast,
                unit=un.lambdas,
                equivalencies=equiv,
            )

            uvw_lambdas = uvw_lambdas.reshape((-1, 3))

            weight_broadcast = da.repeat(weight[:, None, :], nchans, axis=1)
            weight_broadcast *= ~flag

            visibility_data = weight_broadcast * data

            visibility_data = visibility_data.reshape((-1, ncorrs))
            weight_broadcast = weight_broadcast.reshape((-1, ncorrs))

            use_rounding = True if self.ckernel_object is None else False
            u_pix, v_pix = self._calculate_uv_pix(
                uvw_lambdas, self._uvcellsize, self._padded_grid_size, use_rounding=use_rounding
            )

            if self.ckernel_object is None:
                idx, invalid = self._calculate_1d_idx(u_pix, v_pix, self._padded_grid_size)
                # Flagging indexes and visibilities falling outside the grid if any
                idx *= ~invalid
                convolved_weights = weight_broadcast * ~invalid[..., None]
                convolved_visibilities = visibility_data * ~invalid[..., None]

            else:
                # Compute grid indices, validity mask, and effective relative indices.
                idx, invalid, effective_rel_u, effective_rel_v = self.convolve_idx(u_pix, v_pix)
                # Get the oversampled (half) kernel dimensions.
                half_kernel_shape = kernel.shape  # e.g. (5, 5) when half=True
                # Compute the effective 1D indices into the flattened half-kernel.
                effective_idx = da.abs(effective_rel_v
                                       ) * half_kernel_shape[1] + da.abs(effective_rel_u)
                # Use da.take to select the coefficients for each visibility.
                coeffs = da.take(kernel_flat,
                                 effective_idx)[..., None]  # shape: (nvis, kernel_elements, 1)

                convolved_weights = weight_broadcast[:, None, :] * coeffs.real
                convolved_visibilities = visibility_data[:, None, :] * coeffs

                idx *= ~invalid
                convolved_weights *= ~invalid[..., None]
                convolved_visibilities *= ~invalid[..., None]

                idx = idx.reshape(-1)
                convolved_weights = convolved_weights.reshape(-1, ncorrs)
                convolved_visibilities = convolved_visibilities.reshape(-1, ncorrs)

            gridded_weights = weight_bincount(idx, convolved_weights, padded_grid_size)
            gridded_weights = gridded_weights.reshape(
                self._padded_grid_size[0], self._padded_grid_size[1], ncorrs
            )

            gridded_data = complex_bincount(idx, convolved_visibilities, padded_grid_size)
            gridded_data = gridded_data.reshape(
                self._padded_grid_size[0], self._padded_grid_size[1], ncorrs
            )

            for i in range(0, ncorrs):

                if corr_names[i] == "XX":
                    if idx_I:
                        self.uvgridded_visibilities[idx_I[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_I[0]] += gridded_weights[:, :, i]

                    if idx_Q:
                        self.uvgridded_visibilities[idx_Q[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_Q[0]] += gridded_weights[:, :, i]

                elif corr_names[i] == "XY":
                    if idx_U:
                        self.uvgridded_visibilities[idx_U[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_U[0]] += gridded_weights[:, :, i]

                    if idx_V:
                        self.uvgridded_visibilities[idx_V[0]] += (-1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_V[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "YX":
                    if idx_U:
                        self.uvgridded_visibilities[idx_U[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_U[0]] += gridded_weights[:, :, i]

                    if idx_V:
                        self.uvgridded_visibilities[idx_V[0]] += (1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_V[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "YY":
                    if idx_I:
                        self.uvgridded_visibilities[idx_I[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_I[0]] += gridded_weights[:, :, i]

                    if idx_Q:
                        self.uvgridded_visibilities[idx_Q[0]] += (-1.0 * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_Q[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "LL":

                    if idx_I:
                        self.uvgridded_visibilities[idx_I[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_I[0]] += gridded_weights[:, :, i]

                    if idx_V:
                        self.uvgridded_visibilities[idx_V[0]] += (-1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_V[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "LR":
                    if idx_Q:
                        self.uvgridded_visibilities[idx_Q[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_Q[0]] += gridded_weights[:, :, i]

                    if idx_U:
                        self.uvgridded_visibilities[idx_U[0]] += (1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_U[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "RL":
                    if idx_Q:
                        self.uvgridded_visibilities[idx_Q[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_Q[0]] += gridded_weights[:, :, i]

                    if idx_U:
                        self.uvgridded_visibilities[idx_U[0]] += (1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_U[0]] += gridded_weights[:, :, i]
                elif corr_names[i] == "RR":
                    if idx_I:
                        self.uvgridded_visibilities[idx_I[0]] += gridded_data[:, :, i]
                        self.uvgridded_weights[idx_I[0]] += gridded_weights[:, :, i]

                    if idx_V:
                        self.uvgridded_visibilities[idx_V[0]] += (1.0j * gridded_data[:, :, i])
                        self.uvgridded_weights[idx_V[0]] += gridded_weights[:, :, i]
                else:
                    raise ValueError("The correlation does not exist")

        # Normalize gridded visibilities in-place
        sum_wt = da.sum(self.uvgridded_weights, axis=(1, 2))

        da.divide(
            self.uvgridded_visibilities,
            self.uvgridded_weights,
            out=self.uvgridded_visibilities,
            where=self.uvgridded_weights > 0.0
        )

        normalized_weights = self.uvgridded_weights / sum_wt[:, None, None]
        self.sum_wt = da.sum(normalized_weights, axis=(1, 2))
        fft_param = self.uvgridded_visibilities * normalized_weights

        # Make a IFFT, irfft2 and fft2 functions take the last two axes if not given.
        # The sign of the IFFT changes the East-West North-South orientation,
        # therefore, we need to flip by conjugation the resulting Fourier space in the case of hermitian symmetry
        # and use FFT in the case of not using hermitian symmetry

        padded_imsize = tuple(self._padded_imsize)
        padded_grid_size = tuple(self._padded_grid_size)

        dirty_maps = ifft2(
            fft_param,
            s=padded_imsize,
            sign_convention="positive",
            hermitian_symmetry=self.hermitian_symmetry,
            apply_fftshift=True,
            restore_chunks=True
        ) * padded_grid_size[0] * padded_grid_size[1]

        dirty_beams = ifft2(
            normalized_weights,
            s=padded_imsize,
            sign_convention="positive",
            hermitian_symmetry=self.hermitian_symmetry,
            apply_fftshift=True,
            restore_chunks=True
        ) * padded_grid_size[0] * padded_grid_size[1]

        self.max_psf = da.nanmax(dirty_beams.real, axis=(1, 2))

        if self.padding_factor > 1.0:
            dirty_maps = remove_padding(dirty_maps, tuple(self.imsize))
            dirty_beams = remove_padding(dirty_beams, tuple(self.imsize))

        if self.ckernel_object is not None:
            gcf = self.ckernel_object.gcf((self.imsize[0], self.imsize[1]))

            da.divide(
                dirty_maps,
                gcf[np.newaxis, :, :],
                out=dirty_maps,
                where=gcf[np.newaxis, :, :] > 0.0
            )

            da.divide(
                dirty_beams,
                gcf[np.newaxis, :, :],
                out=dirty_beams,
                where=gcf[np.newaxis, :, :] > 0.0
            )

        dirty_maps_images = Image(data=xr.DataArray(dirty_maps), cellsize=self.cellsize)
        dirty_maps_images.create_header(dataset=self.input_data)

        dirty_beams_images = Image(data=xr.DataArray(dirty_beams), cellsize=self.cellsize)
        dirty_beams_images.create_header(dataset=self.input_data)

        return dirty_maps_images, dirty_beams_images
