from dataclasses import dataclass
from typing import Union

import dask.array as da
import numpy as np

from ..wscheme import WeightingScheme


@dataclass(init=False, repr=True)
class Robust(WeightingScheme):
    """Robust weighting scheme class.

    Parameters
    ----------
    robust_parameter : float
        Briggs/Robust parameter
    kwargs :
        WeightingScheme general arguments
    """
    robust_parameter: float = None

    def __init__(self, robust_parameter: float = None, **kwargs):
        super().__init__(**kwargs)
        self.robust_parameter = robust_parameter

    @property
    def robust_parameter(self):
        return self.__robust_parameter

    @robust_parameter.setter
    def robust_parameter(self, val):
        if -2.0 <= val <= 2.0:
            self.__robust_parameter = val
        else:
            raise ValueError("Robust parameter needs to be between [-2.0, 2.0]")

    def calculate_f2(
        self,
        sum_grid_weights_squared: Union[float, np.ndarray, da.Array],
        sum_weights: Union[float, np.ndarray, da.Array],
        robust_parameter: float = None
    ):
        """
        Parameters
        ----------
        sum_grid_weights_squared: Union[float, np.ndarray, da.Array]
            Sum of the gridded weights squared
        sum_weights: Union[float, np.ndarray, da.Array]
            Sum of the non-gridded weights
        robust_parameter: float
            Robust/Briggs parameter

        Returns
        ---------
        The f^2 term in the Briggs equation
        """
        if robust_parameter is None:
            robust_parameter = self.robust_parameter
        num = (5.0 * 10.0**-robust_parameter)**2
        den = sum_grid_weights_squared / sum_weights

        if den.any() == 0.0:
            raise ZeroDivisionError
        else:
            return num / den

    def transform(self) -> None:
        """This function calculates the robust weights and transforms them
        according to this scheme,"""
        if self.gridder is not None:
            dataset = self.input_data
            # max_nu = dataset.spws.max_nu
            original_weight_sum = []
            gridded_weights_squared_sum = []
            not_valid_idxs_list = []
            gridded_weights_list = []
            for ms in dataset.ms_list:
                spw_id = ms.spw_id
                nu = dataset.spws.ref_freqs[spw_id]
                weight = ms.visibilities.weight.data
                _, weights_on_grid, not_valid_idxs, w_k = self.gridder.grid_weights(ms, nu)
                not_valid_idxs_list.append(not_valid_idxs)
                gridded_weights_list.append(w_k)
                # Flagging idx of weights that might fall outside the uv-grid
                original_weight_sum.append(da.sum(weight * ~not_valid_idxs[:, np.newaxis], axis=0))
                # unique_idx = da.unique(idx).compute()
                # gridded_weights = bin_count[unique_idx]  # Non-zero gridded weights on the grid
                gridded_weights_squared_sum.append(da.sum(weights_on_grid**2, axis=0))

            original_weight_sum_array = da.array(original_weight_sum)
            gridded_weights_squared_sum_array = da.array(gridded_weights_squared_sum)
            original_weight_sum = da.sum(original_weight_sum_array, axis=0)
            gridded_weights_squared_sum = da.sum(gridded_weights_squared_sum_array, axis=0)

            f2 = self.calculate_f2(gridded_weights_squared_sum, original_weight_sum)

            for i, ms in enumerate(dataset.ms_list):
                weight = ms.visibilities.weight.data
                imaging_weight = ms.visibilities.imaging_weight
                w_k = gridded_weights_list[i] * ~not_valid_idxs_list[i][:, np.newaxis]
                weight_aux = da.zeros_like(weight)
                den = 1.0 + f2 * w_k
                da.divide(weight, den, out=weight_aux, where=den > 0.0)
                imaging_weight.values = weight_aux

        else:
            raise TypeError("Gridder attribute cannot be None when calculating robust weights")
