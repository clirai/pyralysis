from dataclasses import dataclass

import dask.array as da
import numpy as np

from ..wscheme import WeightingScheme


@dataclass(init=True, repr=True)
class Uniform(WeightingScheme):
    """Uniform weighting scheme class.

    Parameters
    ----------
    kwargs :
        WeightingScheme general arguments
    """

    def transform(self) -> None:
        """This function calculates the uniform weights and transforms them
        according to this scheme,"""
        if self.gridder is not None:
            dataset = self.input_data
            # max_nu = dataset.spws.max_nu
            for ms in dataset.ms_list:
                spw_id = ms.spw_id
                nu = dataset.spws.ref_freqs[spw_id]
                weight = ms.visibilities.imaging_weight
                _, _, not_valid_idxs, w_k = self.gridder.grid_weights(ms, nu)
                weight_aux = da.zeros_like(weight.data)
                grid_weights = w_k * ~not_valid_idxs[:, np.newaxis]
                da.divide(weight.data, grid_weights, out=weight_aux, where=grid_weights > 0.0)
                weight.values = weight_aux
        else:
            raise TypeError("Gridder attribute cannot be None when calculting uniform weights")
