import os
from dataclasses import dataclass
from typing import List, Tuple, Union

import dask.array as da
import numpy as np
import xarray as xr
import zarr
from astropy.units import Quantity, Unit
from multimethod import multimethod

from ...base import (
    Antenna,
    Baseline,
    Dataset,
    Field,
    Observation,
    Polarization,
    SpectralWindow,
    SubMS,
    VisibilitySet,
)
from ...convolution.primary_beam import PrimaryBeam
from ...reconstruction.psf import PSF
from ..io import Io


@dataclass(init=True, repr=True)
class ZarrDataset(Io):
    """IO class to work with Dataset stored in zarr format.

    Stores a Dataset in the following hierarchy:

    /
    ├── antenna
    │   ├── DISH_DIAMETER
    │   ├── FLAG_ROW
    │   ├── ...
    │   └── pb
    │       ├── dish_diameter
    │       └── telescope
    ├── baseline
    │   ├── ANTENNA1
    │   ├── ANTENNA2
    │   └── ...
    ├── field
    │   ├── CODE
    │   ├── DELAY_DIR
    │   ├── PHASE_DIR
    │   ├── REFERENCE_DIR
    │   └── ...
    ├── ms
    │   ├── ms_0
    │   │   └── visibilities
    │   │       ├── DATA
    │   │       ├── UVW
    │   │       ├── WEIGHT
    │   │       └── ...
    │   ├── ms_1
    │   │   └── visibilities
    │   │       └── ...
    │   ├── ...
    │   └── ms_{n}
    │       └── visibilities
    │           └── ...
    ├── observation
    │   ├── FLAG_ROW
    │   ├── OBSERVER
    │   └── ...
    ├── polarization
    │   ├── pol_0
    │   │   ├── CORR_PRODUCT
    │   │   ├── CORR_TYPE
    │   │   └── ...
    │   ├── ...
    │   └── pol_{m}
    │       └── ...
    ├── psf
    └── spectral_window
        ├── spw_0
        │   ├── CHAN_FREQ
        │   ├── CHAN_WIDTH
        │   └── ...
        ├── ...
        └── spw_{l}
            ├── CHAN_FREQ
            ├── CHAN_WIDTH
            └── ...

    Class attributes that are neither arrays nor xr.Dataset are stored as user attributes of the
    zarr group.

    Values of type astropy.units.Quantity are stored as tuples pairs where the first value is the
    float/int/list and the second is the string representation of the unit.

    Parameters
    ----------
    input_name : str, optional
        Input file name used to read a Dataset, by default None.
    output_name : str, optional
        Output file name used to write a Dataset, by default None.
    object_type : str, optional
        Dtype used to coerce arrays with dtype=object in the various xr.Dataset in a Dataset, to
        prevent them from being loaded into memory, by default "U50".

    Attributes
    ----------
    input_name : str
        Input file name used to read a Dataset.
    output_name : str
        Output file name used to write a Dataset.
    object_type : str
        Dtype used to coerce arrays with dtype=object in the various xr.Dataset in a Dataset, to
        prevent them from being loaded into memory.

    See Also
    --------
    Dataset
    """
    object_type: str = "U50"

    def check_zarr_filename(self, filename: str) -> str:
        """Return a file name with the .zarr extension.

        Checks if a file name has the .zarr extension. If it does not, it is added.

        Parameters
        ----------
        filename : str
            File name to check.

        Returns
        -------
        str
            Filename with the .zarr extension.
        """
        if filename.endswith(".zarr"):
            return filename
        return filename + ".zarr"

    def read(
        self,
        filename: Union[str, None] = None,
        store: Union[zarr.storage.Store, None] = None
    ) -> Dataset:
        """Read dataset stored as a zarr group.

        Parameters
        ----------
        filename : Union[str, None], optional
            Path to the zarr group, by default None. If `store` is passed, its use is favored over
            `filename`.
        store : Union[zarr.storage.Store, None], optional
            Store to directory with group, by default None.

        Returns
        -------
        Dataset
            Dataset instance with attributes read from zarr group.

        Raises
        ------
        ValueError
            If there was no file name or store provided to read the dataset.
        ValueError
            If the path provided does not exists.
        ValueError
            If the store provided does not contains a zarr group.
        """
        if store is None:
            if filename is None:
                filename = self.input_name
            if filename is None:
                raise ValueError("No file name was provided to read the dataset")
            store = filename

        if isinstance(store, str) and not os.path.exists(store):
            raise ValueError("Path does not exist")

        if isinstance(store, zarr.storage.Store) and not zarr.storage.contains_group(store):
            raise ValueError("Store does not contains group")

        ds_group = zarr.open_group(store=store, mode="r")

        antenna = self.read_antenna(ds_group)
        baseline = self.read_baseline(ds_group)
        field = self.read_field(ds_group)
        observation = self.read_observation(ds_group)
        polarization = self.read_polarization(ds_group)
        spws = self.read_spectralwindow(ds_group)
        ms_list = self.read_list_subms(ds_group)

        # Bool attributes
        do_psf_calculation = ds_group.attrs.get("do_psf_calculation", True)
        corrected_column_present = ds_group.attrs.get("corrected_column_present", False)

        psf = self.read_psf(ds_group, do_psf_calculation)

        # Dataset
        dataset = Dataset(
            antenna=antenna,
            baseline=baseline,
            field=field,
            spws=spws,
            polarization=polarization,
            observation=observation,
            ms_list=ms_list,
            psf=psf,
            do_psf_calculation=do_psf_calculation,
            corrected_column_present=corrected_column_present,
        )

        return dataset

    def read_antenna(self, group: zarr.Group) -> Union[Antenna, None]:
        """Read group with Antenna and PrimaryBeam data."""
        if "antenna" not in group:
            return None

        antenna = Antenna(dataset=xr.open_zarr(store=group.store, group="antenna"))

        if "pb" not in group.antenna:
            return antenna

        # PrimaryBeam
        pb_kwargs = self._attrs_kwargs_dict(
            group.antenna.pb.attrs, args=["size", "w", "amplitude"], qty_args=["cellsize"]
        )
        primary_beam = PrimaryBeam(
            dish_diameter=da.from_zarr(
                url=group.store.dir_path(), component="antenna/pb/dish_diameter"
            ),
            telescope=da.from_zarr(url=group.store.dir_path(), component="antenna/pb/telescope"),
            **pb_kwargs
        )
        antenna.primary_beam = primary_beam
        return antenna

    def read_baseline(self, group: zarr.Group) -> Union[Baseline, None]:
        """Read group with Baseline data."""
        if "baseline" not in group:
            return None

        baseline = Baseline()
        baseline.dataset = xr.open_zarr(store=group.store, group="baseline")
        bl_kwargs = self._attrs_kwargs_dict(
            group.baseline.attrs, qty_args=["max_baseline", "min_baseline"]
        )
        if "max_baseline" in bl_kwargs:
            baseline.max_baseline = bl_kwargs["max_baseline"]
        if "min_baseline" in bl_kwargs:
            baseline.min_baseline = bl_kwargs["min_baseline"]
        return baseline

    def read_field(self, group: zarr.Group) -> Union[Field, None]:
        """Read group with Field data."""
        if "field" not in group:
            return None

        fld_kwargs = self._attrs_kwargs_dict(
            group.field.attrs, args=["table_keywords", "column_keywords"]
        )
        field = Field(dataset=xr.open_zarr(store=group.store, group="field"), **fld_kwargs)
        return field

    def read_observation(self, group: zarr.Group) -> Union[Observation, None]:
        """Read group with Observation data."""
        if "observation" not in group:
            return None

        observation = Observation(dataset=xr.open_zarr(store=group.store, group="observation"))
        return observation

    def read_polarization(self, group: zarr.Group) -> Union[Polarization, None]:
        """Read group with Polarization data."""
        if "polarization" not in group:
            return None

        n_pol = len(list(group.polarization.group_keys()))
        pol_dataset = []
        for i in range(n_pol):
            pol_dataset.append(xr.open_zarr(store=group.store, group=f"polarization/pol_{i}"))
        polarization = Polarization(dataset=pol_dataset)
        return polarization

    def read_spectralwindow(self, group: zarr.Group) -> Union[SpectralWindow, None]:
        """Read group with SpectralWindow data."""
        if "spectral_window" not in group:
            return None

        n_spws = len(list(group.spectral_window.group_keys()))
        spws_dataset = []
        for i in range(n_spws):
            spws_dataset.append(xr.open_zarr(store=group.store, group=f"spectral_window/spw_{i}"))
        spws = SpectralWindow(dataset=spws_dataset)
        return spws

    def read_list_subms(self, group: zarr.Group) -> Union[List[SubMS], None]:
        """Read group with list of SubMs data."""
        if "ms" not in group:
            return None

        n_sms = len(list(group.ms.group_keys()))
        ms_list = []
        for i in range(n_sms):
            vis = VisibilitySet(
                dataset=xr.open_zarr(store=group.store, group=f"ms/ms_{i}/visibilities")
            )
            ms_kwargs = self._attrs_kwargs_dict(
                group.ms[f"ms_{i}"].attrs,
                args=["_id", "field_id", "polarization_id", "spw_id"],
            )
            ms_list.append(SubMS(visibilities=vis, **ms_kwargs))
        return ms_list

    def read_psf(self, group: zarr.Group, calculate: bool) -> Union[PSF, None]:
        """Read group with PSF data."""
        if calculate or "psf" not in group:
            return None

        psf_kwargs = self._attrs_kwargs_dict(group.psf.attrs, qty_args=["major", "minor", "pa"])
        psf = PSF(**psf_kwargs)
        return psf

    def write(
        self,
        dataset: Dataset,
        filename: Union[str, None] = None,
        store: Union[zarr.storage.Store, None] = None
    ) -> zarr.Group:
        """Write a Dataset as a zarr group.

        Parameters
        ----------
        dataset : Dataset
            Dataset to write.
        filename : Union[str, None], optional
            Path used to create a zarr store, by default None. If `store` is passed, its use is
            favored over `filename`.
        store : Union[zarr.storage.Store, None], optional
            Store to be used for storing the dataset, by default None.

        Returns
        -------
        zarr.Group
            Zarr group containing the dataset.

        Raises
        ------
        ValueError
            If there was no filename or store provided to write the dataset.
        """
        # If path exists, it will overwrite its contents
        if store is None:
            if filename is None:
                filename = self.output_name
            if filename is None:
                raise ValueError("No file name was provided to store the dataset")

            filename = self.check_zarr_filename(filename)
            store = zarr.DirectoryStore(filename)

        # Define zarr group
        ds_group = zarr.group(store=store, overwrite=True)
        for attr, value in dataset:
            if isinstance(value, bool):
                ds_group.attrs[attr] = value
                continue
            self.store_attribute(ds_group, value)

        return ds_group

    @multimethod
    def store_attribute(self, group, attr) -> None:
        """Store each Dataset attribute in a zarr group.

        Parameters
        ----------
        group : zarr.Group
            Base group that will contain each attribute.
        attr : Union[Antenna, Baseline, Field, SpectralWindow, Polarization, Observation,
        List[SubMs], PSF, None]
            Attribute to store its dataset and attributes into a zarr group.

        Raises
        ------
        TypeError
            If an `attr` of an invalid type is passed to the method.
        """
        raise TypeError("Type not supported")

    @store_attribute.register
    def _(self, group: zarr.Group, attr: None) -> None:
        """Method used for undefined (None) Dataset attributes."""
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: Antenna) -> None:
        """Store Antenna dataset and attributes into a zarr group."""
        store = group.store
        dataset_c = self._coerce_dataset_variable(attr.dataset)
        dataset_c.to_zarr(store=store, group="antenna")

        # Primary Beam
        gpb = group["antenna"].create_group("pb")

        # Attributes
        if attr.primary_beam.size is not None:
            gpb.attrs["size"] = attr.primary_beam.size
        if attr.primary_beam.w is not None:
            gpb.attrs["w"] = attr.primary_beam.w
        if attr.primary_beam.cellsize is not None:
            gpb.attrs["cellsize"] = self._quantity_to_tuple(attr.primary_beam.cellsize)
        if attr.primary_beam.amplitude is not None:
            gpb.attrs["amplitude"] = attr.primary_beam.amplitude

        # Arrays
        group_path = f"{store.dir_path()}/{gpb.path}"
        pb_dd = attr.primary_beam.dish_diameter
        pb_t = attr.primary_beam.telescope

        if isinstance(pb_dd, da.Array):
            da.to_zarr(pb_dd, url=group_path, component="dish_diameter")
        else:
            zarr.array(pb_dd, store=store, path=f"{gpb.path}/dish_diameter")

        if isinstance(pb_t, da.Array):
            da.to_zarr(pb_t, url=group_path, component="telescope")
        else:
            zarr.array(pb_t, store=store, path=f"{gpb.path}/telescope")

        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: Baseline) -> None:
        """Store Baseline dataset and attributes into a zarr group."""
        attr.dataset.to_zarr(store=group.store, group="baseline")
        # Store attributes
        if attr.max_baseline is not None:
            group["baseline"].attrs["max_baseline"] = self._quantity_to_tuple(attr.max_baseline)
        if attr.min_baseline is not None:
            group["baseline"].attrs["min_baseline"] = self._quantity_to_tuple(attr.min_baseline)
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: Field) -> None:
        """Store Field dataset and attributes into a zarr group."""
        dataset_c = self._coerce_dataset_variable(attr.dataset)
        dataset_c.to_zarr(store=group.store, group="field")
        # Store attributes
        if attr.table_keywords is not None:
            group["field"].attrs["table_keywords"] = self._serialize_dictionary(attr.table_keywords)
        if attr.column_keywords is not None:
            group["field"].attrs["column_keywords"] = self._serialize_dictionary(
                attr.column_keywords
            )
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: SpectralWindow) -> None:
        """Store SpectralWindow dataset into a zarr group."""
        for i, ds in enumerate(attr.dataset):
            ds_c = self._coerce_dataset_variable(ds)
            ds_c.to_zarr(store=group.store, group=f"spectral_window/spw_{i}")
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: Polarization) -> None:
        """Store Polarization dataset into a zarr group."""
        for i, ds in enumerate(attr.dataset):
            ds.to_zarr(store=group.store, group=f"polarization/pol_{i}")
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: Observation) -> None:
        """Store Observation dataset into a zarr group."""
        dataset_c = self._coerce_dataset_variable(attr.dataset)
        dataset_c.to_zarr(store=group.store, group="observation")
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: List[SubMS]) -> None:
        """Store list of SubMS datasets and attributes into a zarr group."""
        store = group.store
        for i, subms in enumerate(attr):
            vis_dataset = subms.visibilities.dataset
            if "MODEL_DATA" not in vis_dataset:
                model = subms.visibilities.model.data
                vis_dataset = vis_dataset.assign(MODEL_DATA=(["row", "chan", "corr"], model))
            if "CORRECTED_DATA" not in vis_dataset:
                corrt = subms.visibilities.corrected.data
                vis_dataset = vis_dataset.assign(CORRECTED_DATA=(["row", "chan", "corr"], corrt))
            vis_dataset.to_zarr(store=store, group=f"ms/ms_{i}/visibilities")
            if subms._id is not None:
                group["ms"][f"ms_{i}"].attrs["_id"] = subms._id
            if subms.field_id is not None:
                group["ms"][f"ms_{i}"].attrs["field_id"] = subms.field_id
            if subms.polarization_id is not None:
                group["ms"][f"ms_{i}"].attrs["polarization_id"] = subms.polarization_id
            if subms.spw_id is not None:
                group["ms"][f"ms_{i}"].attrs["spw_id"] = subms.spw_id
        return

    @store_attribute.register
    def _(self, group: zarr.Group, attr: PSF) -> None:
        """Store PSF attributes into a zarr group."""
        gpsf = group.create_group("psf")
        gpsf.attrs["major"] = self._quantity_to_tuple(attr.major)
        gpsf.attrs["minor"] = self._quantity_to_tuple(attr.minor)
        gpsf.attrs["pa"] = self._quantity_to_tuple(attr.pa)
        return

    def _quantity_to_tuple(self, qty: Quantity) -> Tuple:
        value = qty.value
        if isinstance(value, np.ndarray):
            value = value.tolist()
        if isinstance(value, da.Array):
            value = value.compute().tolist()
        unit_str = qty.unit.to_string()
        return value, unit_str

    def _tuple_to_quantity(self, tup: Union[Tuple, None]) -> Quantity:
        if tup is None:
            return None
        value, unit_str = tup
        if isinstance(value, list):
            value = np.array(value)
        return value * Unit(unit_str)

    def _coerce_dataset_variable(self, dataset: xr.Dataset) -> xr.Dataset:
        response = dataset.copy()
        for i in response.data_vars:
            if response[i].dtype == "object":
                response[i] = response[i].astype(self.object_type)
        return response

    def _serialize_dictionary(self, dictionary: dict) -> dict:
        sd = dict(dictionary)  # serialized dictionary
        for k, v in sd.items():
            if isinstance(v, np.ndarray):
                sd[k] = v.tolist()
            elif isinstance(v, da.Array):
                sd[k] = v.compute().tolist()
            elif isinstance(v, dict):
                sd[k] = self._serialize_dictionary(v)
        return sd

    def _attrs_kwargs_dict(
        self,
        attrs: dict,
        args: Union[List, None] = None,
        qty_args: Union[List, None] = None
    ) -> dict:
        """Read and create kwargs dict with only available attributes."""
        kwargs = {}
        if args is not None:
            for a in args:
                if attrs.get(a) is not None:
                    kwargs[a] = attrs.get(a)
        if qty_args is not None:
            for q in qty_args:
                if attrs.get(q) is not None:
                    kwargs[q] = self._tuple_to_quantity(attrs.get(q))
        return kwargs
