import logging
from dataclasses import dataclass, field
from logging import Logger

from .field import Field
from .polarization import Polarization
from .spectral_window import SpectralWindow
from .visibility_set import VisibilitySet


@dataclass(init=True, repr=True)
class SubMS:
    """Class that represents the partitioned Measurement Set. Remember that
    dask-ms partitions the whole MS into Fields and Spectral Windows.

    Parameters
    ----------
    _id : int
         ID for each MS partition
    field_id : Field
           Field ID of this MS partition
    polarization_id : Polarization
                  Polarization ID for this MS partition
    spectral_window_id : SpectralWindow
                     Spectral Window ID for this MS partition
    visibilities : VisibilitySet
                  VisibilitySet object for this MS partition
    """
    _id: int = None
    field_id: int = None
    polarization_id: int = None
    spw_id: int = None
    visibilities: VisibilitySet = None

    logger: Logger = field(init=False, repr=False)

    def __post_init__(self):
        self.logger = logging.getLogger(self.__class__.__name__)
        self.logger.setLevel(logging.INFO)
