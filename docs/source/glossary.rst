Glossary
========

- **RML (Regularized Maximum Likelihood)**: An imaging method.
- **Gridding**: Interpolating Fourier space data onto a uniform grid.
- **Degridding**: Estimating model visibilities from gridded images.
