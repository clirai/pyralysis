from unittest.mock import Mock

import dask.array as da
import numpy as np
import pytest
import xarray as xr

from pyralysis.optimization.fi import L1Norm
from pyralysis.optimization.linesearch import BacktrackingArmijo
from pyralysis.optimization.objective_function import ObjectiveFunction
from pyralysis.reconstruction import Image


class TestBacktrackingArmijo:

    @pytest.fixture
    def image(self):
        data = xr.DataArray(np.ones((3, 3, 2)))
        _image = Image(data=data, cellsize=1.0)
        return _image

    def test_init_empty(self):
        f = BacktrackingArmijo()
        assert f.objective_function is None
        assert f.step is None
        assert f.contraction == 0.5
        assert f.decrease == 0.5
        assert f.tol == 1.0e-7
        assert f.max_iter == 100

    def test_init_step(self):
        f = BacktrackingArmijo(step=0.01)
        assert f.objective_function is None
        assert f.step == 0.01
        assert f.contraction == 0.5
        assert f.decrease == 0.5
        assert f.tol == 1.0e-7
        assert f.max_iter == 100

    def test_search(self, image):
        func = [L1Norm(image=image)]
        obj_func = ObjectiveFunction(fi_list=func)
        # old_value keeps the old objective function value
        old_value = obj_func.calculate_function()
        obj_func.calculate_gradient(iteration=100)
        # old_grad contains the gradient
        old_grad = obj_func.dphi
        f = BacktrackingArmijo(objective_function=obj_func, step=100.0)
        assert f.step == 100.0
        assert f.contraction == 0.5
        assert f.decrease == 0.5
        # Armijo backtracking algorithm runs to find a step that minimizes the objective function
        res = f.search(x=image)
        assert len(res) == 2
        # Data for a new image is created
        new_image_data = image.data - res[1] * old_grad
        # New image is set for all fi
        obj_func.fi_image(Image(data=new_image_data, cellsize=image.cellsize))
        # This new value should be lower than old_value
        new_value = obj_func.calculate_function()
        assert res[1] <= 100.0
        assert new_value <= old_value
        assert f.step == res[1]

    def test_search_kwargs(self, image):
        func = [L1Norm(image=image)]
        obj_func = ObjectiveFunction(fi_list=func)
        # old_value keeps the old objective function value
        old_value = obj_func.calculate_function()
        obj_func.calculate_gradient(iteration=100)
        # old_grad contains the gradient
        old_grad = obj_func.dphi
        f = BacktrackingArmijo(objective_function=obj_func)
        assert f.step is None
        assert f.contraction == 0.5
        assert f.decrease == 0.5
        # Armijo backtracking algorithm runs to find a step that minimizes the objective function
        res = f.search(x=image, step=100.0, contraction=1e-5, decrease=1e-5)
        assert len(res) == 2
        # Data for a new image is created
        new_image_data = image.data - res[1] * old_grad
        # New image is set for all fi
        obj_func.fi_image(Image(data=new_image_data, cellsize=image.cellsize))
        # This new value should be lower than old_value
        new_value = obj_func.calculate_function()
        assert res[1] <= 100.0
        assert f.step == res[1]
        assert new_value <= old_value
        assert f.contraction == 1e-5
        assert f.decrease == 1e-5

    @pytest.mark.parametrize(
        "mask_data", [
            (
                da.array(
                    [[[0, 0], [1, 1], [0, 0]], [[0, 0], [1, 1], [0, 0]], [[0, 0], [1, 1], [0, 0]]],
                    dtype=bool
                )
            ),
            (
                da.array(
                    [[[0, 0], [1, 1], [0, 0]], [[1, 1], [1, 1], [1, 1]], [[0, 0], [1, 1], [0, 0]]],
                    dtype=bool
                )
            )
        ]
    )
    def test_search_mask(self, image, mock_mask, mask_data):
        func = [L1Norm(image=image)]
        obj_func = ObjectiveFunction(fi_list=func)
        # Mock mask
        mask = mock_mask()
        mask.data = mask_data
        # old_value keeps the old objective function value
        old_value = obj_func.calculate_function(mask=mask)
        obj_func.calculate_gradient(iteration=100, mask=mask)
        # old_grad contains the gradient
        old_grad = obj_func.dphi
        f = BacktrackingArmijo(objective_function=obj_func, step=100.0)
        # Armijo backtracking algorithm runs to find a step that minimizes the objective function
        res = f.search(x=image, mask=mask)
        assert len(res) == 2
        # Data for a new image is created
        new_image_data = image.data - res[1] * old_grad
        # New image is set for all fi
        obj_func.fi_image(Image(data=new_image_data, cellsize=image.cellsize))
        # This new value should be lower than old_value
        new_value = obj_func.calculate_function(mask=mask)
        assert res[1] <= 100.0
        assert new_value <= old_value
        assert f.step == res[1]
